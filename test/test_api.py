import requests


def test_api():
    """Make a test request to the API."""
    url_to_xml = (
        "https://ahikar-test.sub.uni-goettingen.de/rest/textgrid/tmp/3r82k.me.xml"
    )
    xml = requests.get(url_to_xml).text
    response = requests.post(
        "https://tokenize.ahiqar.sub.uni-goettingen.de/",
        json={"xml": xml},
    )
    print(bytes(str(response.text), "utf-8").decode("unicode_escape"))
    assert isinstance(response.text, str)


if __name__ == "__main__":
    test_api()
