from typing import Dict

from fastapi import FastAPI

from xml_conversion import convert_xml

app = FastAPI()


@app.get("/healthz")
def get_health():
    """Endpoint for K8s liveness probe."""
    return {"status": "ok"}


@app.post("/")
async def tokenize(data: Dict[str, str]):
    """Tokenization method."""
    xml = data["xml"]
    xml = convert_xml(xml)
    return xml
