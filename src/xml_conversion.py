import string
from typing import List

import lxml.etree as ET
from spacy_download import load_spacy

ARABIC_PUNCTUATION = "۔٭؟؞؛؏؎،؍"
SYRIAC_PUNCTUATION = "܀܁܂܃܄܅܆܇܈܉܊܋"
PUNCTUATION = string.punctuation + ARABIC_PUNCTUATION + SYRIAC_PUNCTUATION


def convert_xml(xml: str) -> str:
    """Separate punctuation tokens from word tokens.

    Args:
        xml: XML string with whitespace tokenisation.

    Returns:
        XML string with linguistic tokenisation.

    """
    xml_parser = ET.XMLParser(remove_blank_text=True)
    tree = ET.ElementTree(ET.fromstring(xml, parser=xml_parser))
    texts = []
    for elem in tree.iter():
        if elem.tag.split("}")[-1] == "text":
            for attr in elem.attrib:
                if attr.split("}")[-1] == "lang":
                    texts.append((elem.attrib[attr], []))
        elif elem.tag.split("}")[-1] == "w":
            texts[-1][-1].append(elem)
    for lang, elems in texts:
        words = [elem.text for elem in elems]
        plaintext = " ".join(words)
        language = detect_language(plaintext)  # don't trust the `lang` attribute
        tokens = tokenize_text(plaintext, language)
        i = 0
        for elem in elems:
            tokens_for_word = []
            word = elem.text
            while i < len(tokens) and word.startswith(tokens[i]):
                tokens_for_word.append(tokens[i])
                word = word[len(tokens[i]) :]
                i += 1
            longest_token_k = None
            longest_token_length = -1
            for k, token in enumerate(tokens_for_word):
                length = len([c for c in token if c not in PUNCTUATION])
                if length > longest_token_length:
                    longest_token_k = k
                    longest_token_length = length
            addnexts = []
            for k, token in enumerate(tokens_for_word):
                if k == longest_token_k:
                    elem.text = token
                else:
                    new_elem = ET.Element("w")
                    new_elem.text = token
                    for attr, val in elem.attrib.items():
                        if attr.split("}")[-1] == "id":
                            val += "_" + str(k - longest_token_k)
                        new_elem.set(attr, val)
                    if k < longest_token_k:
                        elem.addprevious(new_elem)
                    elif k > longest_token_k:
                        addnexts.append(new_elem)
            for new_elem in reversed(addnexts):
                elem.addnext(new_elem)
    return ET.tostring(tree, pretty_print=True, encoding="unicode")


def detect_language(text: str) -> str:
    """Detect the language of a text based on the characters.

    Args:
        text: The text.

    Returns:
        The language ("eng", "ara" or "syr").

    """
    charstats = {"eng": 0, "ara": 0, "syr": 0}
    for c in text:
        ord_c = ord(c)
        if ord_c < 128:
            charstats["eng"] += 1
        elif (
            (ord_c in range(1536, 1792))
            or (ord_c in range(64336, 65022))
            or (ord_c in range(65136, 65280))
        ):
            charstats["ara"] += 1
        elif ord_c in range(1792, 1872):
            charstats["syr"] += 1
    return max(charstats, key=charstats.get)


def tokenize_text(text: str, language: str) -> List[str]:
    """Tokenize a text.

    Args:
        text: The text.

    Returns:
        A list of tokens.

    """
    if language == "eng":
        nlp = load_spacy("en_core_web_lg", exclude=["tagger", "parser", "ner"])
        doc = nlp(text)
        return [token.text for token in doc]
    else:
        return tokenize_text_punct(text)


def tokenize_text_punct(text: str) -> List[str]:
    """Tokenize a text at white space.
        Punctuation marks are treated as individual tokens.

    Args:
        text: The text.

    Returns:
        A list of tokens.

    """
    tokens = []
    for token in text.strip().split():
        if len([c for c in token if c in PUNCTUATION]) == len(token):
            tokens.append(token)
        else:
            for c in PUNCTUATION:
                token = token.replace(c, " " + c + " ")
            tokens.extend(token.strip().split())
    return tokens
