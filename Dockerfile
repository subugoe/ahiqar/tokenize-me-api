FROM python:3

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./src /app

CMD ["uvicorn", "api:app", "--host", "0.0.0.0", "--port", "8084"]
